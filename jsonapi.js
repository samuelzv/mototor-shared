"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SCHEMAS = {
    DEALERS: 'dealers',
    ORDERS: 'orders',
    CUSTOMERS: 'customers',
    ADDRESSES: 'addresses',
    AUTH: 'auth'
};
// id's
exports.IDS = {
    DEALER: 'dealerId',
    ORDER: 'orderId',
    CUSTOMER: 'customerId',
    ADDRESS: 'addressId'
};
//# sourceMappingURL=jsonapi.js.map