export const ENDPOINTS = {
  DEALERS : 'dealers',
  ORDERS : 'orders',
  CUSTOMERS : 'customers',
  ADDRESSES : 'addresses',
  AUTH : 'auth'
};