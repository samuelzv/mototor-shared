export const SCHEMAS = {
  DEALERS : 'dealers',
  ORDERS : 'orders',
  CUSTOMERS : 'customers',
  ADDRESSES : 'addresses',
  AUTH : 'auth'
};


// id's
export const IDS = {
  DEALER : 'dealerId',
  ORDER : 'orderId',
  CUSTOMER : 'customerId',
  ADDRESS : 'addressId'
};
